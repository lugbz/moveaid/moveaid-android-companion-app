package org.lugbz.moveaid.model.response;

import com.google.gson.annotations.SerializedName;

import org.lugbz.moveaid.model.Position;

public class LastPositionResponse extends RestResponse {

    @SerializedName("position")
    private Position lastPosition;

    public Position getLastPosition() {
        return lastPosition;
    }
}
