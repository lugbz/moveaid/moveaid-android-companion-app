package org.lugbz.moveaid.model.request;

import com.google.gson.annotations.SerializedName;

public class RecordLocationRequest {

    @SerializedName("lat")
    private final long latitude;

    @SerializedName("lng")
    private final long longitude;

    public RecordLocationRequest(long latitude, long longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
