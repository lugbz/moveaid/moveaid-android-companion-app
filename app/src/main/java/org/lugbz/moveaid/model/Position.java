package org.lugbz.moveaid.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Position {

    @SerializedName("lat")
    private long latitude;

    @SerializedName("lng")
    private long longitude;

    @SerializedName("timestamp")
    private Date timestamp;

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
