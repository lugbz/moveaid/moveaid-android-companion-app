package org.lugbz.moveaid.service;

import org.lugbz.moveaid.model.response.LastPositionResponse;
import org.lugbz.moveaid.model.response.RestResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RealTimeService {

    @POST("position/submit")
    @FormUrlEncoded
    Call<ResponseBody> recordLocation(@Field("lat") double latitude, @Field("lng") double longitude);

    @GET("position/last")
    Call<LastPositionResponse> getLastPosition();

    @GET("verify")
    Call<RestResponse> verifyToken();
}
