package org.lugbz.moveaid.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationRequest;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.location.LocationListenerCompat;

import org.lugbz.moveaid.ui.activity.LoginActivity;
import org.lugbz.moveaid.util.platform.Prefs;

import java.util.concurrent.Executors;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class LocationLoggingService extends Service implements LocationListener {

    private LocationManager mLocationManager;
    private long mPublishIntervalMillis = 1000 * 5;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mPublishIntervalMillis = Prefs.get(this, "locationIntervalMillis", mPublishIntervalMillis);
        startPingingLocation();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        getRealTimeService().recordLocation(
                location.getLatitude(), location.getLongitude()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Prefs.put(LocationLoggingService.this, "last_location_ping_status", true);
                } else {
                    Prefs.put(LocationLoggingService.this, "last_location_ping_status", false);
                    Prefs.put(LocationLoggingService.this, "last_location_ping_code", response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Prefs.put(LocationLoggingService.this, "last_location_ping_status", false);
                Prefs.put(LocationLoggingService.this, "last_location_ping_code", -1);
            }
        });
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        stopSelf();
    }

    @SuppressLint("MissingPermission")
    private void startPingingLocation() {
        mLocationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                mPublishIntervalMillis,
                0,
                this
        );
    }

    private static RealTimeService getRealTimeService() {
        return MARestAPI.getRealTimeService();
    }
}
