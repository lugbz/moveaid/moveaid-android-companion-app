package org.lugbz.moveaid.service;

import org.lugbz.moveaid.model.request.LoginRequest;
import org.lugbz.moveaid.model.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthService {
    @POST("api/auth/token")
    Call<LoginResponse> login(@Body LoginRequest request);
}
