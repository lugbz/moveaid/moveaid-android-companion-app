package org.lugbz.moveaid.service;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class MARestAPI {

    private static String API_URL = "https://moveaid-staging.herokuapp.com/";

    private static Retrofit sUnauthenticatedRetrofit;
    private static Retrofit sAuthenticatedRetrofit;

    private static AuthService sAuthService;
    private static RealTimeService sRealTimeService;

    private static String sAccessToken;

    static {
        sUnauthenticatedRetrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        sAuthenticatedRetrofit = new Retrofit.Builder()
                .baseUrl(API_URL + "api/companion/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(chain ->
                        chain.proceed(chain.request().newBuilder()
                            .addHeader("Authorization", "Token " + MARestAPI.getAccessToken())
                                .build())).build()).build();
        sAuthService = sUnauthenticatedRetrofit.create(AuthService.class);
        sRealTimeService = sAuthenticatedRetrofit.create(RealTimeService.class);
    }

    public static void setAccessToken(String token) {
        sAccessToken = token;
    }

    public static String getAccessToken() {
        return sAccessToken;
    }

    public static void setApiUrl(String apiUrl) {
        if (!apiUrl.endsWith("/")) {
            apiUrl += "/";
        }
        API_URL = apiUrl;
        sUnauthenticatedRetrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        sAuthenticatedRetrofit = new Retrofit.Builder()
                .baseUrl(API_URL + "api/companion/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(chain ->
                        chain.proceed(chain.request().newBuilder()
                                .addHeader("Authorization", "Token " + MARestAPI.getAccessToken())
                                .build())).build()).build();
        sAuthService = sUnauthenticatedRetrofit.create(AuthService.class);
        sRealTimeService = sAuthenticatedRetrofit.create(RealTimeService.class);
    }

    public static String getApiUrl() {
        return API_URL;
    }

    public static AuthService getAuthService() {
        return sAuthService;
    }

    public static RealTimeService getRealTimeService() {
        return sRealTimeService;
    }
}
