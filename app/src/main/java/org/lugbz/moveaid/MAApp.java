package org.lugbz.moveaid;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;

import org.lugbz.moveaid.service.MARestAPI;
import org.lugbz.moveaid.util.platform.Prefs;

public class MAApp extends Application {

    public static final class Constants {
        public static final class Prefs {
            public static final String SERVER_URL = "serverUrl";
            public static final String ACCESS_TOKEN = "token";
            public static final String LOCATION_COMPONENT_CAN_BE_STARTED = "locationPreconditionsReady";
            public static final String LOCATION_COMPONENT_SHOULD_BE_STARTED = "locationActiveRequested";
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Prefs.exists(this, Constants.Prefs.SERVER_URL)) {
            MARestAPI.setApiUrl(Prefs.get(this, Constants.Prefs.SERVER_URL));
        }
        if (Prefs.exists(this, Constants.Prefs.ACCESS_TOKEN)) {
            MARestAPI.setAccessToken(Prefs.get(this, Constants.Prefs.ACCESS_TOKEN));
        }
    }
}
