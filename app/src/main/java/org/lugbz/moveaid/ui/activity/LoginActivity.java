package org.lugbz.moveaid.ui.activity;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.lugbz.moveaid.MAApp;
import org.lugbz.moveaid.R;
import org.lugbz.moveaid.model.request.LoginRequest;
import org.lugbz.moveaid.model.response.LoginResponse;
import org.lugbz.moveaid.service.AuthService;
import org.lugbz.moveaid.service.MARestAPI;
import org.lugbz.moveaid.util.platform.Prefs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements Callback<LoginResponse> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernameView = findViewById(R.id.activity_login_username);
        final EditText passwordView = findViewById(R.id.activity_login_password);
        final MaterialButton submitButton =
                findViewById(R.id.activity_login_submit);
        submitButton.setOnClickListener(v -> {
            if (usernameView.getText().length() > 0 || passwordView.getText().length() > 0) {
                getAuthService().login(new LoginRequest(
                        usernameView.getText().toString(),
                        passwordView.getText().toString()
                )).enqueue(this);
            }
        });
        final ActivityResultLauncher<ScanOptions> barcodeLauncher =
                registerForActivityResult(
                        new ScanContract(),
                        result -> {
                            if(result.getContents() == null) {
                                Toast.makeText(LoginActivity.this, "Scansione annullata", Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    JSONObject credentials = new JSONObject(result.getContents());
                                    Toast.makeText(
                                            LoginActivity.this,
                                            "Autenticazione in corso...",
                                            Toast.LENGTH_LONG
                                    ).show();
                                    MARestAPI.setApiUrl((String) credentials.get("base_url"));
                                    getAuthService().login(new LoginRequest(
                                            (String) credentials.get("username"),
                                            (String) credentials.get("password")
                                    )).enqueue(LoginActivity.this);
                                } catch (JSONException e) {
                                    Toast.makeText(
                                            LoginActivity.this,
                                            "Impossibile interpretare il QR code",
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        }
                );
        final MaterialButton scanQr = findViewById(R.id.activity_login_scan_qr);
        scanQr.setOnClickListener(v -> {
            ScanOptions options = new ScanOptions();
            options.setBeepEnabled(false);
            options.setOrientationLocked(false);
            options.setPrompt("Scansiona QR code da veicoli");
            barcodeLauncher.launch(options);
        });
    }

    private static AuthService getAuthService() {
        return MARestAPI.getAuthService();
    }

    @Override
    public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
        if (response.body() == null) {
            Toast.makeText(this,
                    R.string.activity_login_generic_error, Toast.LENGTH_SHORT).show();
            return;
        }
        if (response.isSuccessful()) {
            final String accessToken = response.body().getAccessToken();
            MARestAPI.setAccessToken(accessToken);
            Log.d("LoginActivity", "token: " + accessToken);
            Prefs.put(this, MAApp.Constants.Prefs.SERVER_URL, MARestAPI.getApiUrl());
            Prefs.put(this, MAApp.Constants.Prefs.ACCESS_TOKEN, accessToken);
            Toast.makeText(this, R.string.activity_login_success, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, R.string.activity_login_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
        Toast.makeText(this,
                R.string.activity_login_generic_error, Toast.LENGTH_SHORT).show();
        Log.e("onFailure", "LoginActivity", t);
    }
}