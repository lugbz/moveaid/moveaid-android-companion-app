package org.lugbz.moveaid.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;

import org.lugbz.moveaid.BuildConfig;
import org.lugbz.moveaid.MAApp;
import org.lugbz.moveaid.R;
import org.lugbz.moveaid.model.response.RestResponse;
import org.lugbz.moveaid.service.AuthService;
import org.lugbz.moveaid.service.LocationLoggingService;
import org.lugbz.moveaid.service.MARestAPI;
import org.lugbz.moveaid.service.RealTimeService;
import org.lugbz.moveaid.util.platform.Prefs;
import org.lugbz.moveaid.util.ui.Snacks;

import java.io.IOException;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<String[]> locationPermissionRequest;
    private SwitchCompat locationServiceSwitch;
    private AlertDialog locationPermissionRationale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationServiceSwitch = findViewById(R.id.activity_main_location_service_switch);
        if (Prefs.get(MainActivity.this, MAApp.Constants.Prefs.LOCATION_COMPONENT_SHOULD_BE_STARTED) != null) {
            locationServiceSwitch.setChecked(Prefs.get(MainActivity.this, MAApp.Constants.Prefs.LOCATION_COMPONENT_SHOULD_BE_STARTED));
        }
        locationServiceSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("ghj", "switch " + isChecked);
            Prefs.put(MainActivity.this, MAApp.Constants.Prefs.LOCATION_COMPONENT_SHOULD_BE_STARTED, isChecked);
            updateLocationServiceStatus();
        });

        locationPermissionRequest = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
            if (result.containsKey(Manifest.permission.ACCESS_COARSE_LOCATION) && result.containsKey(Manifest.permission.ACCESS_FINE_LOCATION)) {
                final Boolean grantedCoarseLocation = Objects.requireNonNull(
                        result.get(Manifest.permission.ACCESS_COARSE_LOCATION));
                final Boolean grantedFineLocation = Objects.requireNonNull(
                        result.get(Manifest.permission.ACCESS_FINE_LOCATION));
                if (grantedCoarseLocation && grantedFineLocation) {
                    Prefs.put(this, MAApp.Constants.Prefs.LOCATION_COMPONENT_CAN_BE_STARTED, true);
                    if (locationPermissionRationale != null && locationPermissionRationale.isShowing()) {
                        locationPermissionRationale.cancel();
                    }
                } else {
                    Prefs.put(this, MAApp.Constants.Prefs.LOCATION_COMPONENT_CAN_BE_STARTED, false);
                    buildLocationPermissionRationale().show();
                }
                updateLocationServiceStatus();
            }
        });
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        checkUpgradeAvailable();
    }

    private void checkUpgradeAvailable() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://movea.id/android/VERSION")
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                runOnUiThread(() -> Toast.makeText(MainActivity.this,
                        R.string.upgrade_check_failed,
                        Toast.LENGTH_SHORT).show());
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                if (response.body() == null) return;
                String versNo = response.body().string();
                if (!versNo.equals(BuildConfig.VERSION_NAME)) {
                    runOnUiThread(() -> new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.upgrade_available)
                            .setMessage(R.string.upgrade_available_message)
                            .setPositiveButton(R.string.upgrade_now, (dialog, which) -> startActivity(
                                    new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://movea.id/android/app-latest.apk"))
                            ))
                            .create().show());
                }
                else {
                    runOnUiThread(() -> Toast.makeText(MainActivity.this,
                            R.string.upgrade_already_on_latest_release,
                            Toast.LENGTH_SHORT).show());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        verifyToken();
    }
    
    private void updateLocationServiceStatus() {
        final boolean status = Prefs.get(this, MAApp.Constants.Prefs.LOCATION_COMPONENT_SHOULD_BE_STARTED, false) && 
                Prefs.get(this, MAApp.Constants.Prefs.LOCATION_COMPONENT_CAN_BE_STARTED, false);
        if (status) {
            startService(new Intent(MainActivity.this, LocationLoggingService.class));
        } else {
            locationServiceSwitch.setChecked(false);
            stopService(new Intent(MainActivity.this, LocationLoggingService.class));
        }
    }

    private void verifyToken() {
        getMARealTimeService().verifyToken().enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse> call,
                                   @NonNull Response<RestResponse> response) {
                final RestResponse rr = response.body();
                if (response.isSuccessful()) {
                    if (ensureFineLocationPermission()) {
                        updateLocationServiceStatus();
                    }
                } else if (response.code() == 403) {
                    Toast.makeText(MainActivity.this,
                            R.string.activity_main_token_verification_failure,
                            Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                } else if (rr != null && rr.getMessage() != null) {
                    Log.d("MainActivity", String.format("rest code %s: %s (%s)",
                            response.code(), rr.getMessage(), rr.getError()));
                    Toast.makeText(MainActivity.this,
                            rr.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Snacks.longer(
                            findViewById(R.id.activity_main),
                            getString(R.string.activity_main_token_verification_error),
                            getString(R.string.activity_main_token_verification_retry),
                            v -> verifyToken()
                    );
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse> call, @NonNull Throwable t) {
                Snacks.longer(
                        findViewById(R.id.activity_main),
                        getString(R.string.activity_main_token_verification_error),
                        getString(R.string.activity_main_token_verification_retry),
                        v -> verifyToken()
                );
            }
        });
    }

    private boolean ensureFineLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
                        ActivityCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                locationPermissionRequest.launch(new String[] {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                });
            } else {
                locationPermissionRequest.launch(new String[] {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                });
            }
            return false;
        } else {
            Prefs.put(this, MAApp.Constants.Prefs.LOCATION_COMPONENT_CAN_BE_STARTED, true);
            if (locationPermissionRationale != null && locationPermissionRationale.isShowing()) {
                locationPermissionRationale.cancel();
            }
            return true;
        }
    }

    private AlertDialog buildLocationPermissionRationale() {
        if (locationPermissionRationale != null) {
            locationPermissionRationale.cancel();
        }
        return locationPermissionRationale = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_location_permission_rationale_title)
                .setMessage(R.string.dialog_location_permission_rationale_message)
                .setPositiveButton(R.string.dialog_location_permission_rationale_go, (dialog, which) ->
                        startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.fromParts(
                                "package", getPackageName(), null))))
                .setNegativeButton(R.string.dialog_location_permission_rationale_cancel, (dialog, which) -> dialog.dismiss())
                .create();
    }

    private static AuthService getMAAuthService() {
        return MARestAPI.getAuthService();
    }

    private static RealTimeService getMARealTimeService() {
        return MARestAPI.getRealTimeService();
    }
}