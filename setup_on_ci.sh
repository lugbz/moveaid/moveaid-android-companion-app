#!/bin/bash

if ! [ -d cmdline-tools/latest ]; then
  wget --quiet --output-document=cmdlinetools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_CMDLINE_TOOLS}.zip
  unzip -d cmdline-tools cmdlinetools.zip
  mv cmdline-tools/cmdline-tools cmdline-tools/latest
fi
echo y | cmdline-tools/latest/bin/sdkmanager --sdk_root=. "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null
echo y | cmdline-tools/latest/bin/sdkmanager --sdk_root=. "platform-tools" > /dev/null
echo y | cmdline-tools/latest/bin/sdkmanager --sdk_root=. "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null
set +o pipefail
yes | cmdline-tools/latest/bin/sdkmanager --sdk_root=. --licenses
set -o pipefail
