#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 version"
  echo "e.g. $0 0.2-alpha3"
  exit 1
fi

echo -n "$1" > $(dirname `realpath $0`)/VERSION
sed -i "s/versionName.*/versionName \"$1\"/" $(dirname `realpath $0`)/app/build.gradle

git add $(dirname `realpath $0`)/VERSION $(dirname `realpath $0`)/app/build.gradle
git commit -m "Release v$1"
git tag v$1
